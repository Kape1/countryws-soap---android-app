package com.example.countryws;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.InputStream;

/**
 * Singleton class for interacting with the Country Web Service.
 */
public class CountryWS {

    private final String WSDL_URL = "http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL";
    private final String NAME_SPACE = "http://www.oorsprong.org/websamples.countryinfo";
    private static CountryWS instance = null;
    private final String TAG = MainActivity.TAG;

    /**
     * Get instance of the CountryWS singleton object.
     * @return CountryWS singleton instance.
     */
    public static CountryWS getInstance() {
        if (instance == null) {
            instance = new CountryWS();
        }
        return instance;
    }

    private CountryWS() {
        Log.i(TAG, "CountryWS");
    }

    /**
     * Get country's ISO code via name.
     * @param countryName
     * @return Country's ISO code.
     */
    private String getCountryISOCode(String countryName) {
        Log.i(TAG, "getCountryISOCode countryName: " + countryName);
        String method_name = "CountryISOCode";
        String soap_action = "";
        String property_name = "sCountryName";
        SoapSerializationEnvelope envelope = soapRequest(property_name, countryName, method_name, soap_action);
        try {
            String countryISOCode = envelope.getResponse().toString();
            if ("No country found by that name".equals(countryISOCode)) {
                return null;
            }
            else {
                return countryISOCode;
            }
        }
        catch (Exception e) {
            Log.i(TAG, "Error in getCountryISOCode: " + e.toString());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Gets the name of the of the currency via ISO code.
     * @param currencyISOCode
     * @return Name of the currency.
     */
    private String getCurrencyName(String currencyISOCode) {
        Log.i(TAG, "getCurrencyName currencyISOCode: " + currencyISOCode);
        String method_name = "CurrencyName";
        String soap_action = "";
        String property_name = "sCurrencyISOCode";
        SoapSerializationEnvelope envelope = soapRequest(property_name, currencyISOCode, method_name, soap_action);
        try {
            return envelope.getResponse().toString();
        }
        catch (Exception e) {
            Log.i(TAG, "Error in getCurrencyName: " + e.toString());
            e.printStackTrace();
        }
        return null;
    }

    private String capitalize(String word) {
        Log.i(TAG, "capitalize word: " + word);
        return word.toUpperCase().charAt(0) + word.substring(1).toLowerCase();
    }

    private String parseCountryNameFromInput(String input) {
        Log.i(TAG, "parseCountryNameFromInput input: " + input);
        String countryName = input.trim();
        if (countryName.contains(" ")) {
            String[] nameArr = countryName.split(" ");
            countryName = "";
            for (int i = 0; i < nameArr.length; i++) {
                countryName += capitalize(nameArr[i]) + " ";
            }
            countryName = countryName.trim();
        }
        else {
            countryName = capitalize(countryName);
        }
        return countryName;
    }

    /**
     * Get full details of a given country via ISO code or name.
     * @param input
     * @return SOAP envelope response object.
     */
    public SoapObject getFullCountryInfo(String input) {
        Log.i(TAG, "getFullCountryInfo input: " + input);
        if (input.length() > 3) {
            input = getCountryISOCode(parseCountryNameFromInput(input));
            if (input == null) {
                return null;
            }
        }
        String ISOCode = input.toUpperCase();
        String method_name = "FullCountryInfo";
        String soap_action = "";
        String property_name = "sCountryISOCode";
        SoapSerializationEnvelope envelope = soapRequest(property_name, ISOCode, method_name, soap_action);
        try {
            SoapObject soapObject = (SoapObject) envelope.getResponse();
            if (!"Country not found in the database".equals(soapObject.getProperty("sName").toString())) {
                soapObject.addProperty("sCurrencyName", getCurrencyName(soapObject.getProperty("sCurrencyISOCode").toString()));
                return soapObject;
            }
        }
        catch (Exception e) {
            Log.i(TAG, "Error in getFullCountryInfo: " + e.toString());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get country's flag image bitmap from give SOAP response object.
     * @param responseObj
     * @return Bitmap of the flag image.
     */
    public Bitmap getCountryFlagImage(SoapObject responseObj) {
        Log.i(TAG, "getCountryFlagImage responseObj: " + responseObj.toString());
        if (responseObj != null) {
            String url = responseObj.getProperty("sCountryFlag").toString();

            // Fixes a bug in where some of he urls don't end with ".jpg" .
            Log.i(TAG, "CountryFlag url: " + url);
            if (!url.contains(".jpg") && url.length() > 0) {
                int iJpg = url.indexOf("jpg");
                url = url.substring(0, iJpg) + ".jpg";
                Log.i(TAG, "Modified url: " + url);
            }

            try {
                InputStream is = new java.net.URL(url).openStream();
                return BitmapFactory.decodeStream(is);
            }
            catch (Exception e) {
                Log.i(TAG, "Error in getCountryFlagImage: " + e.toString());
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * SOAP request/response handler.
     * @param property_name
     * @param param
     * @param method_name
     * @param soap_action
     * @return SOAP response envelope.
     */
    private SoapSerializationEnvelope soapRequest(String property_name, String param, String method_name, String soap_action) {
        Log.i(TAG, "soapRequest " +
                "property_name: " + property_name +
                ", param: " + param +
                ", method_name" + method_name +
                ", soap_action: " + soap_action);

        SoapObject soapObject = new SoapObject(NAME_SPACE, method_name);
        soapObject.addProperty(property_name, param);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(soapObject);
        HttpTransportSE htse = new HttpTransportSE(WSDL_URL);
        try {
            htse.call(soap_action, envelope);
            return envelope;
        }
        catch (Exception e) {
            Log.i(TAG, "Error in soapRequest: " + e.toString());
            e.printStackTrace();
        }
        return null;
    }
}
