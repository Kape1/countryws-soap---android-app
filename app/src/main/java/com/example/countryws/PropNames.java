package com.example.countryws;

import android.util.Log;

public class PropNames {

    private String countryISOCode;
    private String countryName;
    private String capitalCity;
    private String phoneCode;
    private String continentISOCode;
    private String currencyISOCode;
    private String currencyName;
    private String languageISOCode;
    private String languageName;
    private final String TAG = MainActivity.TAG;
    private MainActivity mainActivity;

    public PropNames(Constants language, MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        Log.i(TAG, "PropNames");
        if (language == Constants.ENG) {
            countryISOCode = getRes(R.string.ISO_Code);
            countryName = getRes(R.string.name);
            capitalCity = getRes(R.string.capitalCity);
            phoneCode = getRes(R.string.phoneCode);
            continentISOCode = getRes(R.string.continentISOCode);
            currencyISOCode = getRes(R.string.currencyISOCode);
            currencyName = getRes(R.string.currencyName);
            languageISOCode = getRes(R.string.languageISOCode);
            languageName = getRes(R.string.languageName);
        }
        else if (language == Constants.FIN) {
            countryISOCode = getRes(R.string.finISO_Code);
            countryName = getRes(R.string.finName);
            capitalCity = getRes(R.string.finCapitalCity);
            phoneCode = getRes(R.string.finPhoneCode);
            continentISOCode = getRes(R.string.finContinentISOCode);
            currencyISOCode = getRes(R.string.finCurrencyISOCode);
            currencyName = getRes(R.string.finCurrencyName);
            languageISOCode = getRes(R.string.finLanguageISOCode);
            languageName = getRes(R.string.finLanguageName);
        }
    }

    private String getRes(int id) {
        return mainActivity.getString(id);
    }

    public String getCountryISOCode() {
        return countryISOCode;
    }

    public void setCountryISOCode(String countryISOCode) {
        this.countryISOCode = countryISOCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCapitalCity() {
        return capitalCity;
    }

    public void setCapitalCity(String capitalCity) {
        this.capitalCity = capitalCity;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getContinentISOCode() {
        return continentISOCode;
    }

    public void setContinentISOCode(String continentISOCode) {
        this.continentISOCode = continentISOCode;
    }

    public String getCurrencyISOCode() {
        return currencyISOCode;
    }

    public void setCurrencyISOCode(String currencyISOCode) {
        this.currencyISOCode = currencyISOCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getLanguageISOCode() {
        return languageISOCode;
    }

    public void setLanguageISOCode(String languageISOCode) {
        this.languageISOCode = languageISOCode;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
}
