package com.example.countryws;

import android.graphics.Bitmap;
import android.util.Log;

public class Country {

    private String countryISOCode;
    private String countryName;
    private String capitalCity;
    private String phoneCode;
    private String continentISOCode;
    private String currencyISOCode;
    private String currencyName;
    private Bitmap countryFlagImage;
    private String languageISOCode;
    private String languageName;
    private final String TAG = MainActivity.TAG;

    public Country() {
        Log.i(TAG, "Country");
    }

    public String getCountryISOCode() {
        return countryISOCode;
    }

    public void setCountryISOCode(String countryISOCode) {
        this.countryISOCode = countryISOCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCapitalCity() {
        return capitalCity;
    }

    public void setCapitalCity(String capitalCity) {
        this.capitalCity = capitalCity;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getContinentISOCode() {
        return continentISOCode;
    }

    public void setContinentISOCode(String continentISOCode) {
        this.continentISOCode = continentISOCode;
    }

    public String getCurrencyISOCode() {
        return currencyISOCode;
    }

    public void setCurrencyISOCode(String currencyISOCode) {
        this.currencyISOCode = currencyISOCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Bitmap getCountryFlagImage() {
        return countryFlagImage;
    }

    public void setCountryFlagImage(Bitmap countryFlagImage) {
        this.countryFlagImage = countryFlagImage;
    }

    public String getLanguageISOCode() {
        return languageISOCode;
    }

    public void setLanguageISOCode(String languageISOCode) {
        this.languageISOCode = languageISOCode;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
}
