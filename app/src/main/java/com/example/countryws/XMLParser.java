package com.example.countryws;

import android.graphics.Bitmap;
import android.util.Log;
import org.ksoap2.serialization.SoapObject;

public class XMLParser {

    private final String TAG = MainActivity.TAG;

    /**
     * Parses the XML from the given SOAP response object and creates a country object.
     * @param response
     */
    public Country parseXML(SoapObject response, Bitmap flagBitMap) {
        Log.i(TAG, "parseXML");
        if (response!= null) {
            try {
                Country country = new Country();
                country.setCountryISOCode(response.getProperty("sISOCode").toString());
                country.setCountryName(response.getProperty("sName").toString());
                country.setCapitalCity(response.getProperty("sCapitalCity").toString());
                country.setPhoneCode(response.getProperty("sPhoneCode").toString());
                country.setContinentISOCode(response.getProperty("sContinentCode").toString());
                country.setCurrencyISOCode(response.getProperty("sCurrencyISOCode").toString());
                country.setCurrencyName(response.getProperty("sCurrencyName").toString());
                country.setCountryFlagImage(flagBitMap);
                SoapObject languagesObj = (SoapObject) response.getProperty("Languages");
                SoapObject languageObj = (SoapObject) languagesObj.getProperty("tLanguage");
                country.setLanguageISOCode(languageObj.getProperty("sISOCode").toString());
                country.setLanguageName(languageObj.getProperty("sName").toString());
                return country;
            } catch (Exception e) {
                Log.i(TAG, "Error in parseXML" + e.toString());
                e.printStackTrace();
            }
        }
        return null;
    }
}
