package com.example.countryws;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class MainActivity extends Activity {

    private Button getCountryButton;
    private EditText inputEditText;
    public static final String TAG = "myTag";
    public Constants selectedLanguage = Constants.ENG;
    private ListView detailsListView;
    private SoapObject responseObj;
    private ImageView flagImageView;
    private Bitmap flagBitMap;
    private Country currentCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        connectMainActivity(this);
        findGUIElements();
    }

    /**
     * Pass MainActivity down to helper classes.
     * @param mainActivity
     */
    private void connectMainActivity(MainActivity mainActivity) {
        Log.i(TAG, "connectMainActivity");
        FinLanguageObject.getInstance().setMainActivity(mainActivity);
        AlertHandler.getInstance().setMainActivity(mainActivity);
    }

    /**
     * Find GUI elements from the view.
     */
    private void findGUIElements() {
        Log.i(TAG, "findGUIElements");
        inputEditText = findViewById(R.id.inputEditText);
        flagImageView = findViewById(R.id.flageImageView);
        detailsListView = findViewById(R.id.detailsListView);
        getCountryButton = findViewById(R.id.getCountryButton);
        setListeners();
    }

    /**
     * Set action listeners for flagImageView, getCountryButton and inputEditText.
     */
    private void setListeners() {
        Log.i(TAG, "setListeners");
        flagImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFlagPopUp();
            }
        });

        getCountryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onGetCountryButtonClick();
            }
        });

        inputEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                switch (i) {
                    case EditorInfo.IME_ACTION_DONE:
                        onGetCountryButtonClick();
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        onGetCountryButtonClick();
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.language_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.engLanguageItem:
                changeLanguage(Constants.ENG);
                break;
            case R.id.finLanguageItem:
                changeLanguage(Constants.FIN);
                break;
        }
        return true;
    }

    /**
     * Hides the keyboard
     */
    private void hideKeyboard() {
        Log.i(TAG, "hideKeyboard");
        try {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        catch (Exception e) {
            Log.i(TAG, "Error in hideKeyboard " + e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Creates async task for sending and receiving the SOAP request/response.
     */
    private void onGetCountryButtonClick() {
        Log.i(TAG, "onGetCountryButtonClick");
        String input = inputEditText.getText().toString();
        if (!Pattern.matches("[0-9]+", input)) {
            hideKeyboard();
            SOAP_Task mt = new SOAP_Task(inputEditText.getText().toString());
            mt.execute();
        }
        else {
            AlertHandler.getInstance().displayAlert(getString(R.string.invalidSearchCriteria));
        }
    }

    /**
     * Async SOAP request/response handler.
     */
    private class SOAP_Task extends AsyncTask {

        String input = "";

        public SOAP_Task(String input) {
            Log.i(TAG, "SOAP_Task");
            this.input = input;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            Log.i(TAG, "SOAP_TASK.doInBackground");
            responseObj = CountryWS.getInstance().getFullCountryInfo(input);
            if (responseObj != null) {
                flagBitMap = CountryWS.getInstance().getCountryFlagImage(responseObj);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            Log.i(TAG, "SOAP_TASK.onPostExecute");
            super.onPostExecute(o);
            if (responseObj != null) {
                parseResponse();
            }
            else {
                AlertHandler.getInstance().displayAlert(getString(R.string.noCountryFound));
            }
        }
    }

    /**
     * Parses the SOAP response object into a Country object.
     */
    private void parseResponse() {
        Log.i(TAG, "parseResponse");
        Country country = new XMLParser().parseXML(responseObj, flagBitMap);
        if (country != null) {
            clearInputText();
            currentCountry = country;
            populateListView();
        }
    }

    /**
     * Populates the listView with the Country object's information.
     */
    private void populateListView() {
        Log.i(TAG, "populateListView");
        if (currentCountry != null) {
            ArrayList<String> list = new ArrayList<>();
            Country c = currentCountry;
            PropNames p = new PropNames(selectedLanguage, this);
            list.add(p.getCountryName() + ": " + c.getCountryName());
            list.add(p.getCountryISOCode() + ": " + c.getCountryISOCode());
            list.add(p.getCapitalCity() + ": " + c.getCapitalCity());
            list.add(p.getPhoneCode() + ": " + c.getPhoneCode());
            list.add(p.getContinentISOCode() + ": " + c.getContinentISOCode());
            list.add(p.getCurrencyName() + ": " + c.getCurrencyName());
            list.add(p.getCurrencyISOCode() + ": " + c.getCurrencyISOCode());
            list.add(p.getLanguageName() + ": " + c.getLanguageName());
            list.add(p.getLanguageISOCode() + ": " + c.getLanguageISOCode());
            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    this, android.R.layout.simple_list_item_1, list);
            flagImageView.setImageBitmap(c.getCountryFlagImage());
            detailsListView.setAdapter(adapter);
        }
    }

    /**
     * Clears the inputText.
     */
    public void clearInputText() {
        Log.i(TAG, "clearInputText");
        this.inputEditText.setText("");
    }

    private void changeLanguage(Constants language) {
        Log.i(TAG, "changeLanguage");
        selectedLanguage = language;
        inputEditText.setHint(FinLanguageObject.getInstance().translateWord(getString(R.string.inputEditText_hint)));
        populateListView();
    }

    /**
     * Opens the FlagPopUp view if the currentCountry is not null.
     */
    private void openFlagPopUp() {
        if (currentCountry != null) {
            FlagPopUp.setFlagLargeBitmap(flagBitMap);
            startActivity(new Intent(MainActivity.this, FlagPopUp.class));
        }
    }
}
