package com.example.countryws;

import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Singleton class for handling all the TOAST alert messages.
 */
public class AlertHandler {

    private MainActivity mainActivity;
    private final String TAG = MainActivity.TAG;
    private static AlertHandler instance;

    public static AlertHandler getInstance() {
        if (instance == null) {
            instance = new AlertHandler();
        }
        return instance;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void displayAlert(String message) {
        Log.i(TAG, "displayAlert: " + message);
        message = FinLanguageObject.getInstance().translateWord(message);
        Toast toast = Toast.makeText(mainActivity.getApplicationContext(), message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
