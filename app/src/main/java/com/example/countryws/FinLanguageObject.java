package com.example.countryws;

import android.util.Log;

public class FinLanguageObject {
    private static FinLanguageObject instance;
    private String TAG = MainActivity.TAG;
    private MainActivity mainActivity;

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public static FinLanguageObject getInstance() {
        if (instance == null) {
            instance = new FinLanguageObject();
        }
        return instance;
    }

    private FinLanguageObject(){

    }

    private boolean compare(int id, String word) {
        Log.i(TAG, "compare");
        return word.equals(getRes(id));
    }

    private String getRes(int id) {
        Log.i(TAG, "getRes");
        return mainActivity.getResources().getString(id);
    }

    public String translateWord(String word) {
        Log.i(TAG, "translateWord");
        if (mainActivity.selectedLanguage == Constants.FIN) {
            if (compare(R.string.noCountryFound, word)) {
                return getRes(R.string.finNoCountryFound);
            }
            else if (compare(R.string.invalidSearchCriteria, word)) {
                return getRes(R.string.finInvalidSearchCriteria);
            }
            else if (compare(R.string.inputEditText_hint, word)) {
                return getRes(R.string.finInputEditText_hint);
            }
        }
        return word;
    }

    /*
    public String translateProp(String word) {
        Log.i(TAG, "translateProp");
        switch (word) {
            case "ISO Code":
                return "ISO-koodi";

            case "Name":
                return "Nimi";

            case "Capital City":
                return "Pääkaupunki";

            case "Phone Code":
                return "Suuntanumero";

            case "Continent Code":
                return "Mantereen osan koodi";

            case "Currency ISO Code":
                return "Valuutan ISO-koodi";

            case "Currency Name":
                return "Valuutan nimi";

            case "Language ISO code":
                return "Kielen ISO-koodi";

            case "Language Name":
                return "Kielen nimi";
        }
        return word;
    }
    */

}
