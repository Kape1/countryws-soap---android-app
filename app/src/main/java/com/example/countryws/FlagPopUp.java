package com.example.countryws;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.ImageView;

public class FlagPopUp extends Activity {

    private ImageView flagLargeImageView;
    private static Bitmap flagLargeBitmap;
    private final String TAG = MainActivity.TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "FlagPopUp.onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flag_popup);
        flagLargeImageView = findViewById(R.id.flagLargeImageView);
        flagLargeImageView.setImageBitmap(flagLargeBitmap);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels, height = dm.heightPixels;
        getWindow().setGravity(Gravity.TOP);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.y = 100;
        getWindow().setLayout(width, (int) (height * 0.4));

    }

    public static void setFlagLargeBitmap(Bitmap flagLargeBitmap) {
        FlagPopUp.flagLargeBitmap = flagLargeBitmap;
    }
}
